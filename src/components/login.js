import React, {useState,useEffect} from 'react'
import {View,Dimensions,StyleSheet,TouchableOpacity,Image,Animated,Alert} from 'react-native'
import auth, { firebase } from '@react-native-firebase/auth';
import {colors} from '../styles/styles'
import {Actions, Reducer} from 'react-native-router-flux'
import logoImage from '../assets/icons/logo.png'


import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';

import { TextInput, Button, Text } from 'react-native-paper';

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialIcons"

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
    webClientId: '<FROM DEVELOPER CONSOLE>', // client ID of type WEB for your server (needed to verify user ID and offline access)
    offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    hostedDomain: '', // specifies a hosted domain restriction
    loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
    forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
    accountName: '', // [Android] specifies an account name on the device that should be used
    iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
})



const Login = () =>{

    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [email,updateEmail] = useState('')
    const [emailValid,updateEmailValid] = useState(true)
    const [password,updatePassword] = useState('')
    const [passwordValid,updatePasswordValid] = useState(true)
    const [loginButtonPressed,updateLoginButtonPressed] = useState(false)
    const [passwordHidden,setPasswordHidden] = useState(true)

    const [displaySignUpScreen,updateDisplaySignUpScreen] = useState(false)

    useEffect(()=>{
        console.log("Password Hidden",passwordHidden)
    },[passwordHidden])

    useEffect(()=>{
        console.log("Email Valid",emailValid)
        console.log("Password Valid",passwordValid)
    },[emailValid,passwordValid])

    // Handle user state changes
    const onAuthStateChanged = user =>{
        setUser(user);
        if (initializing) setInitializing(false);
    }

    const onEmailChange = e =>{
        console.log('Email change ->',e)
        updateEmail(e.toLowerCase())
    }

    const onPasswordChange = e =>{
        // console.log('Password change ->',e)
    }

    const loginPressed = () =>{

        updateLoginButtonPressed(true)

        //there must be an email and password value
        if(email == '' || email == ' ') updateEmailValid(false)
        else updateEmailValid(true)
        if(password == '' || password == ' ') updatePasswordValid(false)
        else updatePasswordValid(true)

        //if email and password have a value...try and sign in to firebase
        if(emailValid && passwordValid){
            auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                console.log('User has signed in!');
                Actions.main()
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    updateEmailValid(false)
                }
            
                if (error.code === 'auth/invalid-email') {
                    updateEmailValid(false)
                    updatePasswordValid(false)
                }
                //password
                if(error.code == 'auth/user-not-found'){
                    updateEmailValid(false)
                    updatePasswordValid(false)
                }
                if(error.code == 'auth/wrong-password'){
                    updateEmailValid(false)
                    updatePasswordValid(false)
                }

                let errorMessages = {
                    "auth/email-already-in-use":"Looks like that email is already being used.",
                    "auth/invalid-email":"Looks like the email & password combination is incorrect.",
                    "auth/user-not-found":"Looks like that user does not exist.",
                    "auth/wrong-password":"Looks like the email & password combination is incorrect."
                }

                Alert.alert(errorMessages[error.code])
                
            });
        } 

        
    }

    const forgotPasswordClicked = () =>{
        
        // firebase.auth().sendPasswordResetEmail("deanrivers2@gmail.com")
        //     .then((response)=>response.json())
        //     .then((data)=>console.log(data))
    }

    // sign in using google.
    // make an account for this email if succesful sign in
    signIn = async () => {
        try {
          await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
          this.setState({ userInfo });
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
          } else {
            // some other error happened
          }
        }
      };

    
    // check if email is valid
    const checkEmail = (email) =>{

        
        let regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(regEmail.test(email)) updateEmailValid(true)
        else updateEmailValid(false)
    }



    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        console.log('sub',subscriber)
        return subscriber; // unsubscribe on unmount
    }, [])

    useEffect(()=>{
        // console.log('Listening to email and pw => ',email,password)
        if(email) checkEmail(email)

        if(password) updatePasswordValid(true)
        else updatePasswordValid(false)
    },[email,password])



    //determine if the login button has been pressed at least once.
    // if not....the borders should not be red
    let emailInput = loginButtonPressed?<TextInput
                                            mode="outlined"
                                            style={[styles.textFields,{borderColor:emailValid?colors.white:'red'}]}
                                            maxLength={40}
                                            onChangeText={(e)=>onEmailChange(e)}
                                            autoCapitaliz='none'
                                            value={email}
                                            dense={true}
                                            outlineColor={emailValid?"white":"red"}
                              
                                            placeholder="Email/Username"
                                            
                                            right={<TextInput.Icon name="email" style={{backgroundColor:'white'}}/>}
                                            // outline="red"
                                            error={emailValid?false:true}
                                            dark={true}
                                            
                                        />:<TextInput
                                        mode="outlined"
                                        style={[styles.textFields,{borderColor:colors.white},{outline:'red'}]}
                                        maxLength={40}
                                        onChangeText={(e)=>onEmailChange(e)}
                                        autoCapitaliz='none'
                                        value={email}
                                        dense={true}
                                        outlineColor={emailValid?"white":"white"}
                                
                                        placeholder="Email/Username"
                                        
                                     
                                        // underlineColor="blue"
                                        right={<TextInput.Icon name="email" style={{backgroundColor:'white'}}/>}
                             
            
                                        // name="ios-person"
                                    />

    let passwordInput = loginButtonPressed?<TextInput
                                                mode="outlined"
                                                style={[styles.textFields,{borderColor:passwordValid?colors.white:'red'}]}
                                                maxLength={40}
                                                onChangeText={text=>updatePassword(text)}
                                                password={true}
                                                secureTextEntry={passwordHidden?true:false}
                                                dense={true}
                                                right={<TextInput.Icon name={passwordHidden?"eye-off":"eye"} onPress={()=>setPasswordHidden(!passwordHidden)} />}
                                                placeholder="Password"
                                                outlineColor={passwordValid?"white":"red"}
                                                error={passwordValid?false:true}
                                            />:<TextInput
                                                mode="outlined"
                                                style={[styles.textFields,{borderColor:colors.white}]}
                                                maxLength={40}
                                                onChangeText={text=>updatePassword(text)}
                                                password={true}
                                                secureTextEntry={passwordHidden?true:false}
                                                dense={true}
                                                right={<TextInput.Icon name={passwordHidden?"eye-off":"eye"} onPress={()=>setPasswordHidden(!passwordHidden)} />}
                                                placeholder="Password"
                                                outlineColor={passwordValid?"white":"white"}

                                                
                                            />

    let emailLogin = 
    <View style={styles.emailLogin}>
        <View style={[styles.logoContainer,{flex:2}]}>
            <Image style={{width:80,height:80}} source={logoImage}/>
            {/* <Text
                style={{
                    fontWeight:'bold',
                    color:colors.white,
                    fontFamily:'DamascusLight',
                    paddingTop:'5%'
                }}
            >Soccer NYC</Text> */}
        </View>
        {/* <View>
            <Text style={{color:'white',fontSize:50,fontWeight:'bold'}} >Login</Text>
        </View> */}
        {/* <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Light}
            onPress={()=>alert('Google Login Pressed')}
            disabled={false} /> */}

        <View style={{flex:2,backgroundColor:'transparent'}}>

        
            <View style={[styles.textFieldsContainer,{flex:1,backgroundColor:'transparent',flexDirection:'column',justifyContent:'flex-start'}]}>
                <View >
                    {/* <Text
                        style={styles.subHeaders}
                    >Email</Text> */}
                    {emailInput}
                </View>
                
                <View>
                    {/* <Text
                        style={styles.subHeaders}
                    >Password</Text> */}
                    {passwordInput}


                    
                </View>

                
                
            </View>

            <View style={[styles.buttonContainer,{flex:2,backgroundColor:'transparent'}]}>

                <View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>

                
                    <Button mode="outlined" compact={true}  onPress={()=>loginPressed()} style={{backgroundColor:'transparent',borderWidth:1,borderColor:"white"}} labelStyle={{color:'white'}}>SIGN IN</Button>
                    {/* <TouchableOpacity style={styles.button} onPress={()=>loginPressed()}>
                        <Text>SIGN IN</Text>
                    </TouchableOpacity> */}

                    <View style={{
                            flex:1,
                            justifyContent:'flex-start',
                            alignItems:'center',
                            // backgroundColor:'green',
                            justifyContent:'center',
                            alignItems:'center'

                        }}>
                            <TouchableOpacity onPress={()=>forgotPasswordClicked()}><Text style={{color:colors.white,textDecorationLine:'none',fontWeight:'normal',textAlign:'left',justifyContent:'flex-start',alignItems:'flex-start'}}>Forgot Password?</Text></TouchableOpacity>
                    </View>
                </View>
                
                
                
                <View style={{
                    flex:2,
                    justifyContent:'center',
                    alignItems:'center'
                }}>
                    <Text style={{color:colors.white}}>Don't have an account? </Text>
                    {/* <Button>SING IN</Button> */}
                    <TouchableOpacity onPress={()=>Actions.signup()}><Text style={{color:colors.white,textDecorationLine:'underline',fontWeight:'bold'}}>Sign Up</Text></TouchableOpacity>
                </View>
            </View>

        </View>

        {/* X<View>
            <TextInput
                label="Email"
                value={"text"}
                onChangeText={() => console.log("Typing...")}
                mode="flat"
                
                // underlineColor="red"
                // selectionColor="red"
                outlineColor="red"
                dense={true}
                
            />
        </View> */}
        
        

        

    </View>

    // let render = user?Actions.main():emailLogin
    let render = emailLogin

    return(
        <View style={styles.loginContainer}>
            {render}
        </View>
    )
}

const styles = StyleSheet.create({
    loginContainer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'black',
        alignItems:'center',
    },
    logoContainer:{
        flex:1,
        // backgroundColor:'red',
        justifyContent:'center',
        alignItems:'center',
        color:'white'
    },  
    titleContainer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'black'
    },  
    mainRender:{

    },
    emailLogin:{
        flex:1,
        justifyContent:'center',
        // backgroundColor:'orange',
        // height:windowHeight/1.5,
        width:windowWidth/1.3

    },
    subHeaders:{
        // marginTop:30,
        // paddingLeft:10,
        fontWeight:'bold',
        color:colors.white,
        fontFamily:'DamascusLight',
    },
    textFields:{
        borderColor:colors.white,
        borderWidth:0,
    
        borderRadius:0,
        color:'white',
        backgroundColor:'transparent',
        // padding:10,
        // fontWeight:'bold',
        // fontSize:20,
        backgroundColor:"white",
        borderColor:'red',
        // height:10

    },
    textFieldsContainer:{
        flex:1,
        justifyContent:'center',
        // alignItems:'center',
        // backgroundColor:'green',
        justifyContent:'space-evenly'
    },
    buttonContainer:{
        flex:1,
        // backgroundColor:'blue',
        justifyContent:'flex-start',

    },
    button:{
        backgroundColor:colors.white,
        // flex:1,
        justifyContent:'center',
        alignItems:'center',
        padding:10,
        marginTop:10
    },
    
})

export default Login